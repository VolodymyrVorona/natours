const mongoose = require('mongoose');
const dotenv = require('dotenv');

dotenv.config();

process.on('uncaughtException', (err) => {
  console.log('UNCAUGHT REFECTION!');
  console.log(err.name, err.message);
  process.exit(1);
});

const app = require('./app');

const { PORT = 3000, DATABASE, DATABASE_PASSWORD } = process.env;

const DB = DATABASE.replace('<PASSWORD>', DATABASE_PASSWORD);

mongoose
  .connect(DB, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  })
  .then(() => console.log('DB connection successful'));

const server = app.listen(PORT, () => {
  console.log(`App running on port ${PORT}`);
});

process.on('unhandledRejection', (err) => {
  console.log(err.name, err.message);
  console.log('UNHANDLED REFECTION!');
  server.close(() => {
    process.exit(1);
  });
});
